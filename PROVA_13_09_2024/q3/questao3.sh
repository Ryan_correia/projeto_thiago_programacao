#!/bin/bash
#3° questão da prova do dia 13/09/2024

fil=$1
prt=1

while IFS= read -r line || [ -n "$lin" ]; do
	if [[ "$lin" == "----" ]]; then
		prt=$((prt+1))
		> parte_${prt}.txt
	fi
	echo "$lin" >> parte_${prt}.txt
done < "$fil"

# Esse teste daqui é para salvar a última parte do arquivo, Thiago.
# Caso o arquivo não termine com ----, conforme especificado.

echo "Divisão concluída. Arquivos criados: parte_1.txt, parte_2.txt ..."
