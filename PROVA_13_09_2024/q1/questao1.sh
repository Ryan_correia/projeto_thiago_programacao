#!/bin/bash
#1° questão da prova do dia 13/09/2024

uid=$(( RANDOM % 100 + 1 ))
rsp=$(curl -s "https://dummyjson.com/users/$uid")


nom=$(echo "$rsp" | jq -r '.firstName')
sob=$(echo "$rsp" | jq -r '.lastName')
usr=$(echo "$rsp" | jq -r '.username')
pas=$(echo "$rsp" | jq -r '.password')
ip1=$(echo "$rsp" | jq -r '.ip')
ema=$(echo "$rsp" | jq -r '.email')

echo "$nom $sob::$usr::$pas::$ip1::$ema"
