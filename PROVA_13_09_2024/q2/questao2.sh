#!/bin/bash
#2° questão da prova do dia 13/09/2024

url="https://raw.githubusercontent.com/linuxacademy/content-elastic-log-samples/master/access.log"

fil="access.log"

# Script referente à questão A

if [ -f "$fil" ]; then
	echo "O arquivo $fil já existe no diretório."
else
	echo "Baixando o arquivo $fil..."
	curl -O "$url"
	echo "Download concluído."
fi

sleep 1

# Script referente à questão B
echo "Geração de todos os IPs, sem repetição, que aparecem neste arquivo: $fil."
sleep 2
grep -oP '(\d{1,3}\.){3}\d{1,3}' "$fil" | sort -u

sleep 2
# Script referente à questão C
echo "Script que exibe um endereço IP aleatório, dentro do arquivo access.log."
grep -oP '(\d{1,3}\.){3}\d{1,3}' "$fil" | sort -u | shuf -n 1
