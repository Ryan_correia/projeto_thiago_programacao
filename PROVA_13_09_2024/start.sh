#!/bin/bash

read -p "Digite o número de questões: " questoes

for ((i=1; i<=$questoes; i++)); do
	mkdir q$i
	touch questao$i.sh
	mv questao$i.sh q$i
	echo "#!/bin/bash" > q$i/questao$i.sh
	chmod +x q$i/questao$i.sh
	echo "#$i° questão da prova do dia 13/09/2024" >> q$i/questao$i.sh
done

