#!/bin/bash
#Exercício 1° de Thiago, aula de 23/08/2024

output_file="passwd.csv"

echo "Username,Password,UID,GID,Full name,Home Directory,Shell" > "$output_file"

awk -F: '{OFS=","; print $1,$2,$3,$4,$5,$6,$7}' /etc/passwd >> "$output_file"

echo "Arquivo $output_file criado com sucesso!"
