#!/bin/bash
#Exercício 4° de Thiago, aula de 23/08/2024

descriptografar() {
	local texto=$1
	local chave_original=$2
	local chave_inversa=$3
	echo "$texto" | tr "$chave_inversa" "$chave"
}

chave_inversa="aqrtsyuqisloapqmzxcqalpAQRTSYUQISLOAPMZXCQALP"
chave_original="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

if [ $# -eq 0 ]; then
	echo "Uso: $0 <texto_a_descriptografar>"
	exit 1
fi

texto_descriptografado=$(descriptografar $1 $chave_original $chave_inversa)

echo "Texto descriptografado: $texto_descriptografado."
