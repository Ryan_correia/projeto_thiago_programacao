#!/bin/bash
#Exercício 2° de Thiago, aula de 23/08/2024

if [ $# -eq 0 ]; then
	echo "Uso: $0 <arquivo_de_entrada>"
	exit 1
fi

input_file="$1"

mkdir diretorio_2

while IFS= read -r nome_completo; do
	nome_arquivo=$(echo "$nome_completo" | tr ' ' '-')
	touch diretorio_2/"$nome_arquivo" 
	echo "Arquivo $nome_arquivo criado."
done < "$input_file"
