#!/bin/bash
#Exercício 3° de Thiago, aula de 23/08/2024

criptografar() {
	local texto="$1"
	local chave="$2"

	echo "$texto" | tr 'a-zA-Z' "$chave"
}

chave="aqrtsyuqisloapqmzxcqalpAQRTSYUQISLOAPMZXCQALP"

if [ $# -eq 0 ]; then
	echo "Uso : $0 <texto_a_criptografar>"
	exit 1
fi

texto_criptografado=$(criptografar "$1" "$chave")

echo "Texto criptografado: $texto_criptografado."
