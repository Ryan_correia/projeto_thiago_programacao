#!/bin/bash

for ((i=1; i<=5; i++)); do
	touch exercicio$i.sh
	chmod +x exercicio$i.sh
	echo "#!/bin/bash" > exercicio$i.sh
	echo "#Exercício $i° de Thiago, aula de 23/08/2024" >> exercicio$i.sh
done
