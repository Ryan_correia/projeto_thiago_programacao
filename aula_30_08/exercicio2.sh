#!/bin/bash

rm arquivo_editado.log

head -n 30 access.log | tail -n +30 access.log > arquivo_editado.log

echo "Usando a paginação, por meio de more: "
more arquivo_editado.log

sleep 10

echo "Usando a paginação, por meio de less: "
less arquivo_editado.log

