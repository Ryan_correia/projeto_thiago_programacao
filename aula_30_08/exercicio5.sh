#!/bin/bash

entrada="verdade"

while [ $entrada == "verdade" ]; do
	read -p "Digite a opção: " input
	case "$input" in
		1) 
			echo "[1] Efetuar Download: "
			URL="https://raw.githubusercontent.com/linuxacademy/content-elastic-log-samples/master/access.log"
			caminho_arquivo="/home/ryan/aulas_bash/exercicios_sala/aula_30_08/$URL"
			if [ -f "$caminho_arquivo" ]; then
				echo "Arquivo já existe, não será baixado!"
			else
			       	echo "Arquivo não existe, será baixado!"
				wget $URL
			fi

			;;
		2)
			echo "[2] Exibir resumo do arquivo: "
			rm arquivo_editado.log

			head -n 30 access.log | tail -n +30 access.log > arquivo_editado.log
			echo "Usando a paginação, por meio de more: "
			more arquivo_editado.log
			
			sleep 10
			
			echo "Usando a paginação, por meio de less: "
			less arquivo_editado.log

			;;
		3)
			echo "[3] Listar IPs: "
			
			rm arquivo_IPs.txt
			
			grep -oP '\b\d{1,3}(?:\.\d{1,3}){3}\b' access.log | sort | uniq >> arquivo_IPs.>

			;;
		4)
			echo "[4] Exibir IP aleatório: "
			
			echo "Endereço IP random: "
			shuf -n 1 arquivo_IPs.txt


			;;
		5)
			echo "[5] Sair: "
			echo "Você digitou a opção para sair: "
			echo "Saindo do arquivo..."
			entrada="false"
			;;
	esac
done
