#!/bin/bash

URL="https://raw.githubusercontent.com/linuxacademy/content-elastic-log-samples/master/access.log"

caminho_arquivo="/home/ryan/aulas_bash/exercicios_sala/aula_30_08/$URL"

if [ -f "$caminho_arquivo" ]; then
	echo "Arquivo já existe, não será baixado!"
else
	echo "Arquivo não existe, será baixado!"
	wget $URL
fi

