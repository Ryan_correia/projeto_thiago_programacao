#!/bin/bash
#1° questão da prova do dia 06/09/2024

#echo "1 -> Verifica se o usuário existe; "
#if id "$us" &> /dev/null; then
#echo "2 -> Verifica se o usuário está logado; "
#if who | grep -q "^$us "; then
#echo "3 -> Lista os arquivos da pasta home do user; "
#ls -l /home/$us

en=$1

while true; do
	case $en in;
		1)
			echo "1 -> Opção para verificar se o user existe;  "
			read -p "Digite seu usuário: " us
			if [[ id "$us" &> /dev/null ]]; then
			       echo "O usuário não existe"
			       echo "Erro nas opções 2 e 3, saindo do programa..."
			       exit 1
		       else
			       echo "O usuário existe!"
			fi	       
			;;
		2)
			echo "2 -> Opção para verificar se o user está logado; "
			read -p "Digite seu usuário: " u1
			if who | grep -q "^$u1 "; then
				echo "O usuário não está logado"
			else
				echo "O usuário está logado no sistema !"
			fi
			;;
		3)
			echo "3 -> Opção para listar os arquivos da pasta home; "
			read -p "Digite seu usuário: " u2
			ls -l /home/$u2
			;;
		4)
			echo "4 -> Opção para sair do loop; "
			exit 0
			;;
	esac
done
