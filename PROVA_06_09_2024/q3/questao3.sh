#!/bin/bash
#3° questão da prova do dia 06/09/2024

aq="$1"

if [ "$1" -eq "-c"]; then
	grep -v '^\s*$' "$2"
	echo "Linhas removidas do arquivo $2"
fi

lb=$(grep -c '^\s*$' "$aq")

echo "Linhas em branco do arquivo: $lb"

