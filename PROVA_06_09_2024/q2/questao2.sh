#!/bin/bash
#2° questão da prova do dia 06/09/2024

if [[ $1 == "a" ]]; then
	echo "a - Lisa de diretórios de uma pasta: "
	ls -d */
elif [[ $1 == "b" ]]; then
	echo "b - Lista de executáveis: "
	find . -maxdepth 1 -type d
elif [[ $1 == "c" ]]; then
	echo "c - Lista de scripts shell: "
	find . -maxdepth 1 -type f -executable
fi
