#!/bin/bash
#5° questão da prova do dia 06/09/2024

s1=$1

c=0

if [[ ! "$s1" =~ [A-Z] ]]; then
	echo "Senha inválida deve conter pelo menos 1 letra maiúscula"
	c=$(c+1)
fi

if [[ ! "$s1" =~ [a-z] ]]; then
	echo "Senha inválida, deve conter pelo menos 1 letra minúscula"
	c=$(c+1)
fi

if [[ ! "$s1" =~ [0-9] ]]; then
	echo "Senha inválida, deve conter pelo menos 1 número"
	c=$(c+1)
fi


if [ $c -eq 3 ]; then
	echo "Senha validada com sucesso, parabéns!"
else
	echo "Sua senha não atingiu os marcadores mínimos!"
fi


