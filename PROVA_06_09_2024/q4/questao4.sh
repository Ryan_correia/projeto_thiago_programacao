#!/bin/bash
#4° questão da prova do dia 06/09/2024

if [ ! -f "$1" ]; then
	echo "Arquivo não encontrado."
	exit 1
fi

grep -o 'R\$[[:space:]]*[0-9]\{1,3\}\(\.[0-9]\{3\}\)*,[0-9]\{2\}' "$1"

