#!/bin/bash

source arquivo1.sh

if [ ! -f "$1" ]; then
	echo "Arquivo $1 não encontrado."
	exit 1
fi

contador=1

while IFS= read -r linha; do
	if (( contador % 2 == 1 )); then
		vermelho "$linha"
	else
		azul "$linha"
	fi
	((contador++))

done < "$1"
