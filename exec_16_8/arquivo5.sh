#!/bin/bash

gerar_ips() {
	local a1=$1 a2=$2 a3=$3 a4=$4
	local b1=$5 b2=$6 b3=$7 b4=$8
}

for i in $(seq $a1 $b1); do
	for j in $(seq $(( i == a1 ? a2 : 0 )) $(( i == b1 ? b2 : 255 ))): do
		for k in $(seq $(( i == a1 && j == a2 ? a3 : 0 )) $(( i == b1 && j == b2 ? b3 : 255 ))); do
			for l in $(seq $(( i == a1 && j == a2 && k == a3 ? a4 : 0 )) $((i == b1 && j == b2 && k == b3 ? b4 : 255 ))); do
				echo "$i.$j.$k.$l"
			done
		done
	done
done

gerar_ips_no_intervalo 192 168 1 1 192 168 1 10
