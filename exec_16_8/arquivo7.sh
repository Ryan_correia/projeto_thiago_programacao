#!/bin/bash

source ./cores.sh
source ./gera_ips.sh

if [ $# -ne 8 ]; then
    echo "Uso: $0 A1 A2 A3 A4 B1 B2 B3 B4"
    echo "Exemplo: $0 192 168 1 1 192 168 1 10"
    exit 1
fi

arquivo_saida="resultado_ping.txt"
> "$arquivo_saida" # Limpa o arquivo de saída

for ip in $(gera_ips_no_intervalo "$@"); do
    echo "Pinging $ip..."
    ping -c 1 "$ip" >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        mensagem="SUCESSO: $ip"
        verde "$mensagem"
    else
        mensagem="FALHA: $ip"
        vermelho "$mensagem"
    fi
    echo "$mensagem" >> "$arquivo_saida"
done

