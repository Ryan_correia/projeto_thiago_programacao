#!/bin/bash

source ./arquivo2.sh

gerar_num_aleatorio() {
	local a=$1
	local b=$2
	echo $((RANDOM % (b - a + 1) + a))

}

escolher_string() {
	local lista=("$@")
	local tamanho=${#lista[@]}
	local indice=$(gerar_num_aleatorio 0 $((tamanho - 1)))
	echo "${lista[$indice]}"
}

cores=("vermelho" "verde" "azul" "amarelo" "magenta" "azul_claro")

while IFS= read -r linha; do
	cor=$(escolher_string "${cores[@]}")
	$cor "$linha"
done < "$1"
