#!/bin/bash

vermelho() {
	echo -e "\e[31m$1\e[0m"
}

verde() {
	echo -e "\e[32m$1\e[0m"
}

azul() {
	echo -e "\e[34m$1\e[0m"
}

amarelo() {
	echo -e "\e[33m$1\e[0m"
}

magenta() {
	echo -e "\e[35m$1\e[0m"
}

azul_claro() {
	echo -e "\e[36m$1\e[0m"
}

vermelho "Esse é um texto em vermelho."
verde "Esse é um texto em verde."
azul "Esse é um texto em azul."
amarelo "Esse é um texto em amarelo."
magenta "Esse é um texto em magenta."
azul_claro "Esse é um texto em azul claro."
