#!/bin/bash

gerar_num_aleatorio() {
	local a=$1
	local b=$2
	echo $((RANDOM % (b - a + 1) + a))
}

escolhe_string() {
	local lista=("$a")
	local tamanho=${lista[@]}
	local indice=$(gerar_num_aleatorio 0 $((tamanho -1)))
	echo "${lista[$indice]}"
}

echo "Digite o primeiro num: "
read num1

echo "Digite o segundo num: "
read num2

num=$(gerar_num_aleatorio $num1 $num2) 
echo "Número aleatório entre $num1 e $num2: $num"

str=$(escolhe_string "maçã" "thiago" "pão" "café" "bash" "shell")
echo "Fruta escolhida aleatoriamente: $str."
